#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                         #
# Settup kubernetes packaging in debian 11 (aka bullseye) #
#                                                         #
#                                                         #
# script writer: fpc7063@gmail.com                        #
#                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# TODO:
#https://docs.mirantis.com/mcr/20.10/install/mcr-linux/ubuntu.html
# Configuration
# Container Runtime may be "containerd" "cri-o" 
CONTAINER_RUNTIME=containerd
# NODE_TYPE can be "MAIN" "MASTER" "WORKER"
NODE_TYPE=MAIN
K8S_VERSION=v1.24.3-00
eval $(cat /etc/os-release)
OS_FULL_NAME=${ID}:${VERSION_CODENAME}
OS_DISTRIBUTION=${ID}
OS_FLAVOR={VERSION_CODENAME}


###################################################################################
# Remove all swap from server
echo "[DEBUG]: Removing swap"
SWAP_LIST=$(swapon -s | sed '1d' | cut -f1)
swapoff ${SWAP_LIST} &> /dev/null
sed -Ei 's/(.*swap.*)/#\1/g' /etc/fstab


###################################################################################
# Init
echo "[DEBUG]: check for running as root"
if [[ $(whoami) != "root" ]]; then
    echo "Please, run the script as root"
    exit 1
fi
echo "[DEBUG]: Updating repositories"
apt-get update > /dev/null


###################################################################################
# Install container utilities
echo "[DEBUG]: Removing any residual container packaging"
apt-get remove -y docker docker-engine docker.io containerd runc cri-o cri-o-runc > /dev/null

if [[ ${CONTAINER_RUNTIME} == "containerd" ]]; then
    echo "[DEBUG]: Installing containerd"
    apt-get install -y ca-certificates curl gnupg lsb-release gpg > /dev/null
    mkdir -p /etc/apt/keyrings > /dev/null
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg > /dev/null
    chmod a+r /etc/apt/keyrings/docker.gpg > /dev/null
    echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt-get update > /dev/null
    apt-get install -y containerd.io > /dev/null

    # Overriding default docker debian default /etc/containerd/config.toml
    [[ ${OS_DISTRIBUTION} == "ubuntu" ]]; mkdir /etc/containerd
    cat <<EOF | tee /etc/containerd/config.toml
version = 2
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
runtime_type = "io.containerd.runc.v2"
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
SystemdCgroup = true
EOF
    systemctl restart containerd

elif [[ ${CONTAINER_RUNTIME} == "cri-o" ]]; then
    echo "[DEBUG]: Installing cri-o"
    apt-get install -y curl gnupg2 > /dev/null
    OS=Debian_10
    VERSION=1.22
    echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
    echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list
    curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | apt-key add -
    curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | apt-key add -
    apt-get update > /dev/null
    apt-get install -y cri-o cri-o-runc > /dev/null

    cat <<EOF | tee /etc/crio/crio.conf.d/02-cgroup-manager.conf
[crio.runtime]
conmon_cgroup = "pod"
cgroup_manager = "cgroupfs"
EOF
    systemctl enable crio --now
    systemctl start crio
fi


###################################################################################
# Install k8s
echo "[DEBUG]: Installing kubernetes packages"
apt-get install -y apt-transport-https ca-certificates curl ebtables ethtool > /dev/null
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg > /dev/null
chmod a+r /usr/share/keyrings/kubernetes-archive-keyring.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
if [ ${NODE_TYPE} == "MAIN" ] || [ ${NODE_TYPE} == "MASTER" ]; then
    apt-get update > /dev/null
    apt-get install -y kubectl=${K8S_VERSION} > /dev/null
    apt-get install -y kubelet=${K8S_VERSION} > /dev/null
    apt-get install -y kubeadm=${K8S_VERSION} > /dev/null
    apt-mark hold kubelet kubeadm kubectl > /dev/null
elif [[ ${NODE_TYPE} == "WORKER" ]]; then
    apt-get update > /dev/null
    apt-get install -y kubelet=${K8S_VERSION} kubeadm=${K8S_VERSION} kubectl=${K8S_VERSION} > /dev/null
    apt-mark hold kubelet kubeadm kubectl > /dev/null
fi


###################################################################################
# Settup k8s networking
echo "[DEBUG]: Setting up networking configuration"
cat <<EOF | tee /etc/modules-load.d/k8s.conf
br_netfilter
overlay
EOF

cat <<EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sysctl --system > /dev/null

modprobe br_netfilter
modprobe overlay


###################################################################################
# Setup bash_completion
echo "[DEBUG]: Setting up bash_completion"
apt-get install -y bash-completion > /dev/null
kubeadm completion bash | tee /etc/profile.d/kubeadm.sh > /dev/null
kubectl completion bash | tee /etc/profile.d/kubectl.sh > /dev/null


###################################################################################
# Attempt images pull from gcr.io
echo "[DEBUG]: Pulling images with kubeadm"
kubeadm config images pull


exit 0